//
//  Tab1ViewController.swift
//  MatDes
//
//  Created by Anthony Krasnov on 29.10.2017.
//  Copyright © 2017 Anthony Krasnov. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class Tab1ViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    var photo: [String] = []
    
    var post: Post?
    var comment: Comment?
    var users: [User] = [] {
        didSet{
            if users.count == 5 {
                tableView?.reloadData()
            }
        }
    }
    
    var tableView: UITableView?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tbvc", for: indexPath)
        cell.textLabel?.text = users[indexPath.row].username
        cell.detailTextLabel?.text = users[indexPath.row].email
        return cell
    }
    
    
    let mainItems = ["Posts", "Comments", "Users", "Photos", "Todos"]
    var items = [24 + 8 + 14 + 8, 24 + 8 + 14 + 8, 0, 0,20]
    var selectedIndexPath: IndexPath?
    var layout: UICollectionViewFlowLayout?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = UIColor.white
        collectionView!.register(PostCell.self, forCellWithReuseIdentifier: mainItems[0])
        collectionView!.register(CommentCell.self, forCellWithReuseIdentifier: mainItems[1])
                collectionView!.register(UserCell.self, forCellWithReuseIdentifier: mainItems[2])
                collectionView!.register(PhotoCell.self, forCellWithReuseIdentifier: mainItems[3])
                collectionView!.register(TodoCell.self, forCellWithReuseIdentifier: mainItems[4])
        
        
        layout = UICollectionViewFlowLayout()
        
        for i in 1..<6 {
            Service.shared.fetchUser(by: i, completion: { (user) in
                self.users.append(user)
            })
        }
        //        tableView?.reloadData()
        Service.shared.fetchPhoto(by: 3) { (photo) in
            self.photo = [photo.url!]
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainItems.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellID = mainItems[indexPath.row]
        switch cellID {
        case "Posts":
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! PostCell
            cell.cellDelegate = self
            cell.indexPath = indexPath
            if post != nil {
                cell.bodyLabel.text = post?.body
                cell.titleLabel.text = post?.title
            }
            return cell
        case "Comments":
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! CommentCell
            cell.cellDelegate = self
            cell.indexPath = indexPath
            if comment != nil {
                cell.bodyLabel.text = comment?.body
                cell.emailLabel.text = comment?.email
                cell.nameLabel.text = comment?.name
            }
            return cell
        case "Users":
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainItems[indexPath.row], for: indexPath) as! UserCell
            cell.tableView.delegate = self
            cell.tableView.dataSource = self
            cell.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "tbvc")
            tableView = cell.tableView
            return cell
        case "Photos":
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainItems[indexPath.row], for: indexPath) as! PhotoCell
            return cell
        default:
1
